Name:           perl-Object-HashBase
Version:        0.009
Release:        1
Summary:        Build hash-based classes
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/Object-HashBase
Source0:        https://cpan.metacpan.org/authors/id/E/EX/EXODIST/Object-HashBase-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(:VERSION) >= 5.8.1
 
%description
This package is used to generate classes based on hash references. Using this
class will give you a new() method, as well as generating accessors you
request.  Generated accessors will be getters, set_ACCESSOR setters will also
be generated for you. You also get constants for each accessor (all caps)
which return the key into the hash for that accessor. Single inheritance is
also supported.
 
%package tools
Summary:        Generate inlined Object::HashBase Perl module
Requires:       %{name} = %{version}-%{release}
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))
Requires:       perl(Test::More) >= 0.98
 
%description tools
hashbase_inc.pl script generates a Perl module that contains
a Object::HashBase module mangled into a name space of your choice. It can
also generate the tests for it.
 
%prep
%setup -q -n Object-HashBase-%{version}
 
%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}
 
%install
%{make_install}
%{_fixperms} $RPM_BUILD_ROOT/*
 
%check
make test
 
%files
%license LICENSE
%doc Changes README
%{perl_vendorlib}/*
%exclude %{perl_vendorlib}/Object/HashBase
%{_mandir}/man3/*
%exclude %{_mandir}/man3/Object::HashBase::*
 
%files tools
%{_bindir}/hashbase_inc.pl
%{perl_vendorlib}/Object/HashBase
%{_mandir}/man3/Object::HashBase::*
 
%changelog
* Mon Aug 3 2020 dingyue <dingyue5@huawei.com> -0.009 -1
- Package init
